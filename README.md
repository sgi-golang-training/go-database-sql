Example from https://dasarpemrogramangolang.novalagung.com/A-sql.html

Step 1: 
```
cd go-database-sql
go get github.com go-sql-driver/mysql
```

Step 2:
Prepare MySQL Table named dbgo
```
CREATE TABLE IF NOT EXISTS `tb_student` (
  `id` varchar(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_student` (`id`, `name`, `age`, `grade`) VALUES
('B001', 'Jason Bourne', 29, 1),
('B002', 'James Bond', 27, 1),
('E001', 'Ethan Hunt', 27, 2),
('W001', 'John Wick', 28, 2);

ALTER TABLE `tb_student` ADD PRIMARY KEY (`id`);
```

Step 3:
Set credential
```
db, err := sql.Open("mysql", "<mysql username>:<mysql password>@tcp(127.0.0.1:3306)/dbgo")
```

Step 4:
```
go run main.go
```
